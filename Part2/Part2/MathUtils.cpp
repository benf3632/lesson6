#include "MathUtils.h"

double MathUtils::calPentagonArea(double side)
{
	double numerator = 5 * pow(side, 2) * tan(54);
	return numerator / 4;
}

double MathUtils::calHexagonArea(double side)
{
	double numerator = 3 * sqrt(3) * pow(side, 2);
	return numerator / 2;
}