#pragma once
#include <exception>

class InputException : public std::exception {
public:
	virtual char const* what() const;
};