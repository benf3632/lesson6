#include "Pentagon.h"

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "side is " << _side << std::endl << "Area: " << MathUtils::calPentagonArea(_side) << std::endl;;
}

void Pentagon::setSide(double side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	_side = side;
}

Pentagon::Pentagon(std::string color, std::string name, double side) : Shape(color, name)
{
	setSide(side);
}