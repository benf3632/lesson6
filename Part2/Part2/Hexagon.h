#pragma once
#include "shape.h"
#include "shapeexception.h"
#include "MathUtils.h"

class Hexagon : public Shape {
private:
	double _side;
public:
	Hexagon(std::string color, std::string name, double side);
	virtual void draw();
	void setSide(double side);
};