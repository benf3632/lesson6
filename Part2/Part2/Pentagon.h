#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Pentagon : public Shape {
private:
	double _side;
public:
	virtual void draw();
	void setSide(double side);
	Pentagon(std::string color, std::string name, double side);
};
