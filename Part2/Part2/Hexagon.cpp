#include "Hexagon.h"

Hexagon::Hexagon(std::string color, std::string name, double side) : Shape(color, name)
{
	setSide(side);
}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "side is " << _side << std::endl << "Area: " << MathUtils::calPentagonArea(_side) << std::endl;;
}

void Hexagon::setSide(double side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	_side = side;
}