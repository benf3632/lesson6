#include <iostream>
#define ERROR8200 8200

int add(int a, int b, bool* exc) {
	if (a+b == 8200)
	{
		*exc = true;
	}

	if (!*exc)
	{
		return a + b;
	}
}

int  multiply(int a, int b, bool* exc) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, exc);
	if (i == ERROR8200)
	{
		*exc = true;
	}
  };
  if (!*exc)
  {
	  return sum;
  }
}

int  pow(int a, int b, bool* exc) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a, exc);
	if (exponent == ERROR8200 || i == ERROR8200)
	{
		*exc = true;
	}
  }
  if (!*exc)
  {
	  return exponent;
  }
}

int main(void) {
	bool exc = false;
	if (multiply(4100, 2, &exc) && !exc)
	{
		std::cout << multiply(4100, 2, &exc) << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	exc = false;
	if (add(4100, 4100, &exc) && !exc)
	{
		std::cout << add(4100, 4100, &exc) << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	exc = false;
	if (pow(5, 2, &exc)  && !exc)
	{
		std::cout << pow(5, 2, &exc) << std::endl;
	}
	else
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
	}
	
	
	system("pause");
}